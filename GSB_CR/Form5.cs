﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSB_CR
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void btnPraticien_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.FormClosing += new FormClosingEventHandler(form5_FormClosing);
            this.Hide();
            form1.Show();
        }

        private void form5_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Close();
        }

        private void btnVisiteur_Click(object sender, EventArgs e)
        {
            Form6 form6 = new Form6();
            form6.FormClosing += new FormClosingEventHandler(form5_FormClosing);
            this.Hide();
            form6.Show();
        }
    }
}
