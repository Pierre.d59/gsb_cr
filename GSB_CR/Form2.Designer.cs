﻿namespace GSB_CR
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonMedicament = new System.Windows.Forms.Button();
            this.buttonPraticien = new System.Windows.Forms.Button();
            this.buttonVisiteur = new System.Windows.Forms.Button();
            this.buttonCR = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(731, 69);
            this.label1.TabIndex = 4;
            this.label1.Text = "Accueil";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(731, 37);
            this.label3.TabIndex = 6;
            this.label3.Text = "Bienvenue";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(37, 235);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 36);
            this.label2.TabIndex = 7;
            this.label2.Text = "Gestion des comptes\r\n            rendus";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(207, 235);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 36);
            this.label4.TabIndex = 8;
            this.label4.Text = "Gestion des praticiens";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(386, 235);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 36);
            this.label5.TabIndex = 9;
            this.label5.Text = "Gestion des visiteurs";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(547, 235);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(176, 36);
            this.label6.TabIndex = 10;
            this.label6.Text = "Gestion des médicaments";
            // 
            // buttonMedicament
            // 
            this.buttonMedicament.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMedicament.Image = ((System.Drawing.Image)(resources.GetObject("buttonMedicament.Image")));
            this.buttonMedicament.Location = new System.Drawing.Point(599, 157);
            this.buttonMedicament.Name = "buttonMedicament";
            this.buttonMedicament.Size = new System.Drawing.Size(75, 75);
            this.buttonMedicament.TabIndex = 3;
            this.buttonMedicament.UseVisualStyleBackColor = true;
            // 
            // buttonPraticien
            // 
            this.buttonPraticien.Image = ((System.Drawing.Image)(resources.GetObject("buttonPraticien.Image")));
            this.buttonPraticien.Location = new System.Drawing.Point(247, 157);
            this.buttonPraticien.Name = "buttonPraticien";
            this.buttonPraticien.Size = new System.Drawing.Size(75, 75);
            this.buttonPraticien.TabIndex = 2;
            this.buttonPraticien.UseVisualStyleBackColor = true;
            this.buttonPraticien.Click += new System.EventHandler(this.buttonPraticien_Click);
            // 
            // buttonVisiteur
            // 
            this.buttonVisiteur.Image = ((System.Drawing.Image)(resources.GetObject("buttonVisiteur.Image")));
            this.buttonVisiteur.Location = new System.Drawing.Point(422, 157);
            this.buttonVisiteur.Name = "buttonVisiteur";
            this.buttonVisiteur.Size = new System.Drawing.Size(75, 75);
            this.buttonVisiteur.TabIndex = 1;
            this.buttonVisiteur.UseVisualStyleBackColor = true;
            // 
            // buttonCR
            // 
            this.buttonCR.Image = ((System.Drawing.Image)(resources.GetObject("buttonCR.Image")));
            this.buttonCR.Location = new System.Drawing.Point(72, 157);
            this.buttonCR.Name = "buttonCR";
            this.buttonCR.Size = new System.Drawing.Size(75, 75);
            this.buttonCR.TabIndex = 0;
            this.buttonCR.UseVisualStyleBackColor = true;
            this.buttonCR.Click += new System.EventHandler(this.buttonCR_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(209)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(755, 291);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonMedicament);
            this.Controls.Add(this.buttonPraticien);
            this.Controls.Add(this.buttonVisiteur);
            this.Controls.Add(this.buttonCR);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Accueil";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form2_FormClosed);
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCR;
        private System.Windows.Forms.Button buttonVisiteur;
        private System.Windows.Forms.Button buttonPraticien;
        private System.Windows.Forms.Button buttonMedicament;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}